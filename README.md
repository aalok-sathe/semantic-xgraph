## Semantic Solver for High-res and Zero-res language pairs

This repository is intended to house a set of tools and a general
program to solve linguistic _semantic correspondence_ problems.



### Formalization of a morpho-semantics correspondence problem

A correspondence problem *P* is defined using *n+1* lists
corresponding to languages `L_0` ... `L_n`:
- a list of tokens in language `L_0` of size *k_0*, or simply, *k*
- a list of tokens in language `L_1` of size *k_1*
- ...
- a list of tokens in language `L_n` of size *k_n*

in such a way that `L_0` is the _reference_ language, or the
language we (or the computer) _know_.
With respect to the computer, this would translate to having
available one or more models of
word-, character-, and phoneme-level embeddings in `L_0`, captured
for use in multiple NLP tasks, not all same.
We then say that we _know_ nothing about languages `L_1` ... `L_n`.
However, in reality for a human trying to solve a similar
problem, this is hardly ever true: we usually know just about
enough about the concepts of morpho-semantics to be able to
expect certain kinds of morphological behavior out of the
structure of any arbitrary language `L_i`.

Particularly, correspondence problems in natural languages tend to
involve a morphological component: morphology helps form links
in unknown language(s) and compare it with the reference language.

We expect that for all pairs of lists of languages `L_i`, `L_j`,
there must some set `S_ij` such that `S_ij` is a subset of the
tokens list of `L_i` and that of `L_j`, as supplied
in the input of the problem. This subset must have length greater
than or equal to zero.
`|S_ij|` would be approximately the same size
as the size of `list_i` and/or `list_j`. In a typical solution, a set
`S_ij` would be returned such that
`|S_ij| = min(|list_i|, |list_j|)`.
In such a case, we would like to find out
an ordered list of tokens `t_i1`, `t_i2`, ... in `list_i`, and a
corresponding ordered list of tokens `t_j1`, `t_j2`, ... such that
among the combinations between the given tokens, pairwise
combinations of tokens `t_i` and `t_j` is the best semantic
correspondence between the two languages. Typically one language
would be _known_, whereas the other, unknown, to the solver.

### Approaching such a problem

#### Dealing with *L*
##### 1. Word embeddings and Euclidean vector distance
Since we would have access to model of the reference language, we
could compute word embeddings for tokens in the reference language.
`forall` token `t_i` in _L_, let `v_i` be a vector representing a
_d_-dimensional embedding of `t_i`. We could then compute pairwise
Euclidean distances between tokens: `forall` tokens `t_i, t_j` in
*L*, the distance *d_ij* = `dist(t_i, t_j)`.
We would then be able to construct a graph *G* with a vertex for each
token in *L*, and edges with weights *d_ij* between the vertices.

##### 2. ???

#### Dealing with another language, *L'*

##### 1. Levenshtein distance
Since tokens are strings, we can compute the minimum edit distance
pairwise between all the tokens, and make a graph with tokens as
vertices and Ld as edge weights. Then try to establish closest
isomorphism with the graph we formed with *L* (NP-complete?)
